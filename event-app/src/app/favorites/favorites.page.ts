import { Component, OnInit } from "@angular/core";
import { ApolloService } from "src/app/services/apollo.service";
import { StorageService } from "src/app/services/storage.service";
import { AuthenticationService } from "../services/authentication.service";
import { Router, NavigationEnd, ActivatedRoute } from "@angular/router";
import { CompactEvent } from "../feed/feed.page";

export interface Favorites {
  favorites: Array<CompactEvent>;
}

@Component({
  selector: "app-favorites",
  templateUrl: "./favorites.page.html",
  styleUrls: ["./favorites.page.scss"]
})
export class FavoritesPage implements OnInit {
  favorites: Array<CompactEvent> = Array();
  loading = false;

  constructor(
    private apolloService: ApolloService,
    private storage: StorageService,
    private router: Router,
    private activatedRouter: ActivatedRoute
  ) {}

  // ngOnInit() {
  //   this.getFavorites()
  // }

  async ngOnInit() {
    await this.onEnter();

    this.router.events.subscribe(event => {
      if (
        event instanceof NavigationEnd &&
        event.url === "/members/tabs/favorites"
      ) {
        this.onEnter();
      }
    });

    this.activatedRouter.queryParams.subscribe(params => {
      if (params.createdEvent && params.eventTitle && params.eventId) {
        this.onEnter();
      }
    });
  }

  async onEnter() {
    this.getFavorites();
  }

  async getFavorites() {
    this.loading = true;
    try {
      const authInfo = await this.storage.getAuthInfo();

      this.favorites = await this.apolloService.getFavorites(authInfo.id);

      console.log(this.favorites);
      this.loading = false;
    } catch (e) {
      console.log("caught error - " + e);
    }
  }

  goToEventDetails(id) {
    this.router.navigate([`members/event-details/${id}`]);
  }
}
