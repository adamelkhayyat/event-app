### Hybrid Development - Ionic

Authors:   
Adam El-Khayyat (@adamelkhayyat)   
Cage Spence (@cagespence)   

If you're a new developer, please check out the wiki pages below to get an idea of how to work on this project.       
Additional resources are also available below (for support/etc).

## Background Info

Event allows a user to create events, and find events based on their interests. Interests are based around a set of predefined tags in the app, which are shared between users and events. 

Users are able to easily get started with the app, and start interacting with the community by joining, and commenting on events.

## Prerequisites

* Run the [backend](https://gitlab.com/saxion.nl/aad/backend/assignment4/13)
* Edit `src\environments\environment.ts` and add your current ip address `const baseUrl = "https://<your ip address>:8443";`
* Node.js  [download LTS here.](https://nodejs.org/en/)
* Ionic & Cordova installed
```bash
npm install -g ionic cordova
```

## Install Guide

```bash
npm install
```

run on android
```bash
ionic cordova run android
```

run on ios
```bash
ionic cordova run ios
```

## Important Wiki Pages
* [Deploying App on Device (and connecting local API)](https://gitlab.com/saxion.nl/aad/hybrid/assignment3/02/wikis/Hardware-Deployment)
* [SSL Issues troubleshooting guide)](https://gitlab.com/saxion.nl/aad/hybrid/assignment3/02/-/wiki_pages/apollo-troubleshooting)
* [How to use Apollo](https://gitlab.com/saxion.nl/aad/hybrid/assignment3/02/wikis/apollo)
* [Debug an Ionic App via Chrome dev tools](https://medium.com/@coderonfleek/debugging-an-ionic-android-app-using-chrome-dev-tools-6e139b79e8d2)

## Other useful resources
* [Ionic Forums](https://forum.ionicframework.com/)
* [Ionic Docs](https://ionicframework.com/docs)
* [Ionic UI Components](https://ionicframework.com/docs/components)
* [Cordova Docs](https://cordova.apache.org/docs/en/latest/)